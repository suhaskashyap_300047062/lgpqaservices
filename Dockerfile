FROM python
COPY . /myntra
RUN pip install mmh3
RUN pip install redis
RUN pip install redis-py-cluster
RUN pip install elasticsearch
RUN pip install flask
RUN pip install pika
RUN pip install cassandra-driver
CMD ["python", "/myntra/src/qa-service.py"]


