from __future__ import print_function, unicode_literals
from __future__ import absolute_import, division
#import commands
import mmh3
import sys
from rediscluster import RedisCluster
from flask import Flask,jsonify,request,json
import json
import hashlib
import pika
import socket
from cassandra.cluster import Cluster
from elasticsearch import Elasticsearch

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

app = Flask(__name__)

# Get the redis object for th notification id mentioned
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/pushstatus/<host>/<port>/<notification_id>')
def get_notification_status(host, port, notification_id):
    comm = ""
    status = {}
    print(host)
    r = RedisCluster(host, int(port))
    print( notification_id)
    status = r.hgetall('n_' + base64.b64encode(mmh3.hash_bytes('nanlyt_nid_%s' % notification_id))[:-2])
    return json.dumps(status)

# Get capping information
# Parameter:
#     notification_id: Notification Id whose response object needs to be retrieved from Redis
@app.route('/notification/capping/<host>/<port>/<key>')
def get_notification_capping_status(host, port, key):
    comm = ""
    status = {}
    r = RedisCluster(host, int(port))
    k =  "n_" + base64.b64encode(mmh3.hash_bytes(key))[:-2]
    smember_set = r.smembers(k)
    l = list(smember_set)
    return json.dumps(l, ensure_ascii=False)

@app.route('/freequencycap/<cardType>/<uidx>/<storyID>')
def get_freequency_capping_currentStatus(uidx,storyID,cardType):
    host=''
    try:
         request.args.get('environment').encode('utf-8')
    except Exception as ex:
         return jsonify({"Message":"Pass the environment Query Parameter.Value can be stage,qa or fully qualified host name ","Status":"Error"})
    if request.args.get('environment').encode('utf-8').lower()=='qa':
        host = 'qa_lg2'
    elif request.args.get('environment').encode('utf-8').lower() == 'dockins':
        host ='d7lgpredis.myntra.com'
    else:
        host =request.args.get('environment').encode('utf-8')
    try:
        r = RedisCluster(host,7002)
    except:
         return jsonify({"error":"Can not able to connect to the host:"+host})
    key = uidx+':' + cardType +':' + storyID
    status= r.get(computeMD5hash(key))
    remainingTime = r.ttl(computeMD5hash(key))
    return jsonify({"status":status,"remainingTime":remainingTime})

def computeMD5hash(key):
    m = hashlib.md5()
    m.update(key.encode('utf-8'))
    return m.hexdigest()

@app.route('/feedgenerate/<uidx>')
def feedGeneration(uidx):
    host=''
    try:
         request.args.get('environment').encode('utf-8')
    except Exception as ex:
         return jsonify({"Message":"Pass the environment Query Parameter.Value can be stage,qa or fully qualified host name ","Status":"Error"})
    if request.args.get('environment').encode('utf-8').lower()=='qa':
        hostName = 'qa_lg2'
    elif request.args.get('environment').encode('utf-8').lower() == 'dockins':
        hostName ='d7lgprabbitmq.myntra.com'
    else:
        hostName =request.args.get('environment').encode('utf-8')
    try:    
        connection = pika.BlockingConnection(pika.ConnectionParameters(
          host=hostName))
        channel = connection.channel()
        channel.basic_publish(exchange='activity_engine',
                     routing_key='UserFeedGeneration',
                     body=json.dumps([uidx]))
    except Exception as excep:
           return jsonify({"status":"Failed","cause":str(excep),"statusMessage":"Failed to generate the Feed for the user "+uidx})
    connection.close()
    return jsonify({"status":"Success","statusMessage":"Feed Generated successfully for the user "+uidx})


@app.route('/story/<objectType>/<objectID>/<uidx>')
def getStoryID(objectType,objectID,uidx):
    #try:
     #  host = "qa_lg2"
      # ip=socket.gethostbyname(host)
      # print "Connecting to %s" %(str(ip))
       #cluster = Cluster([str(ip)])
       #session = cluster.connect("lgp")
       #print "connected"
    #except Exception,e:
     #     print e
     #     return json.dumps(e)
    host=''
    try:
         request.args.get('environment').encode('utf-8')
    except Exception as ex:
         return jsonify({"Message":"Pass the environment Query Parameter.Value can be stage,qa or fully qualified host name ","Status":"Error"})
    if request.args.get('environment').encode('utf-8').lower() == 'qa':
       host ='qa_lg2'
    elif request.args.get('environment').encode('utf-8').lower() == 'dockins':
       host = 'd7lgpcassandra.myntra.com'
    else:
       host = request.args.get('environment').encode('utf-8')
    print(host)
    session = connectCassandra(host,'lgp')
    query=""
    if objectType=="targeted" or objectType=="onboarding":
       query="select objectdata from user_target_feed where userid='%s' and feed_type='%s'and seqno=0 and objectid='%s'"%(uidx,objectType,objectID)
       print("Executing the Query %s" %(query))
    elif objectType=="sponsored":
         #getObjectES('story_fumes','story_fumes_doc',objectID)
         print("*** Inside Sponsored ****")
         conn = httplib.HTTPSConnection("localhost:8083")
         conn.request("GET", "/elasticsearch/story_fumes/story_fumes_doc/"+objectID+"?environment="+request.args.get('environment').encode('utf-8'))
         res = conn.getresponse()
         response = res.read()
         return response
         '''URL ='http://localhost:8083/elasticsearch/story_fumes/story_fumes_doc/'+objectID+'?environment='+request.args.get('environment').encode('utf-8')
         print URL
         try:
             resp = request.get(URL)
         except Exception as e:
            print(e)
         print resp.text
         print("****At the end of sponsored")
         return resp.text'''
    elif objectType=="ads":
         #getObjectES('ads','object',objectID)
         conn = httplib.HTTPSConnection("http://localhost:8083")
         conn.request("GET", "/elasticsearch/ads/object/"+objectID+"?environment="+request.args.get('environment').encode('utf-8'))
         res = conn.getresponse()
         response = res.read()
         return response
    elif objectType=="organic":
        query="select objectdata from user_organic_feed where userid='%s'" %(uidx)
        print("Executing the Query %s" %(query))
    try:
         rows=session.execute(query)
         print(rows)
    except Exception as e:
            return jsonify({"exception occured while executing query":"error","exception":str(e)})
    print("Executed query")
    print("object data in db %s\n" %(rows))
    objectdataJson = json.dumps(rows)
    objectdata = objectdataJson.replace("\\","").replace("[[\"","").replace("\"]]","")
    if objectdata == '[]':
        response = app.response_class(
        response=json.dumps({"Message":"Record Not found"}),
        status=200,
        mimetype='application/json'
    )
        return response
    else:
        response = app.response_class(
        response=objectdata,
        status=200,
        mimetype='application/json'
    ) 
        return response

def connectCassandra(host,keyspace):
   try:
      print(host +" "+keyspace)
      ip=socket.gethostbyname(str(host))
      print("Connecting to %s" %(str(ip)))
      cluster = Cluster([str(ip)])
      session = cluster.connect(keyspace)
      print("Connection Successful")
   except Exception as e:
         print(e)
         return e
   return session

@app.route('/elasticsearch/<index>/<docType>/<objectId>')
def getObjectES(index,docType,objectId):
   host=''
   try:
         request.args.get('environment').encode('utf-8')
   except Exception as ex:
         return jsonify({"Message":"Pass the environment Query Parameter.Value can be stage,qa or fully qualified host name ","Status":"Error"})
   if request.args.get('environment').encode('utf-8').lower() == 'qa':
      host ='qa_lg3'
   elif request.args.get('environment').encode('utf-8').lower() == 'dockins':
      host = 'd7lgpes.myntra.com'
   else:
      host = request.args.get('environment').encode('utf-8')
   try:
      print("Connecting to "+index)
      es=Elasticsearch([host],port=9200)
     # es=Elasticsearch(['qa_lg3'],sniff_on_start=True,sniff_on_connection_fail=True,sniffer_timeout=60)
      appID = objectId.split(':')
      res = es.search(index=index, doc_type=docType, body={"query": {"match": {"object_id": objectId,"app_id":appID[0]}}})
   except Exception as e:
         print(e)
         return e
   print("Searched the ES Successfully")
   return jsonify(res)



if __name__ == "__main__":
   app.run(host='0.0.0.0',port=8083)
